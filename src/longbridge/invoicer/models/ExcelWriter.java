/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.models;

import java.awt.Point;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author chigozirim
 */
public class ExcelWriter {

    //Invoice workbook
    public static Workbook mainResultWorkbook;
    //The sheet we will work with representing the Invoice
    public static Sheet mainSheet;
    //The file where the invoice is to be written
    public static File newSpreadSheet;

    private static final File template = new File("template.xls");

    private static final HashMap<String, Point> fieldMappings = new HashMap<>();

    /**
     *
     * @param saveFile
     * @param invoice
     * @param invoiceEntries
     */
    public static void writeInvoiceToExcel(File saveFile, Invoice invoice, List<InvoiceEntry> invoiceEntries) {
        try {
            //Files.copy(template.toPath(), saveFile.toPath(), new CopyOption[]{COPY_ATTRIBUTES, REPLACE_EXISTING});
            Workbook invoiceWorkBook = WorkbookFactory.create(template);
            Sheet invoiceSheet = invoiceWorkBook.getSheetAt(0);
            Row row = null;
            int startRow = 8;
            row = invoiceSheet.getRow(startRow);
            row.getCell(7).setCellValue((invoice.getDateCreated() == null) ? "" : invoice.getDateCreated().replace("-", "/"));
            row.getCell(5).setCellValue(invoice.getInvoiceNumber());
            startRow += 5;
            row = invoiceSheet.getRow(startRow);
            row.getCell(3).setCellValue((invoice.getBillTo1() == null) ? "" : invoice.getBillTo1());
            row.getCell(6).setCellValue((invoice.getShipTo1() == null) ? "" : invoice.getShipTo1());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(3).setCellValue((invoice.getBillTo2() == null) ? "" : invoice.getBillTo2());
            row.getCell(6).setCellValue((invoice.getShipTo2() == null) ? "" : invoice.getShipTo2());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(3).setCellValue((invoice.getBillTo3() == null) ? "" : invoice.getBillTo3());
            row.getCell(6).setCellValue((invoice.getShipTo3() == null) ? "" : invoice.getShipTo3());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(3).setCellValue((invoice.getBillTo4() == null) ? "" : invoice.getBillTo4());
            row.getCell(6).setCellValue((invoice.getShipTo4() == null) ? "" : invoice.getShipTo4());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(3).setCellValue((invoice.getBillTo5() == null) ? "" : invoice.getBillTo5());
            row.getCell(6).setCellValue((invoice.getShipTo5() == null) ? "" : invoice.getShipTo5());

            startRow += 2;
            row = invoiceSheet.getRow(startRow);
            row.getCell(3).setCellValue((invoice.getBillToAttn() == null) ? "" : invoice.getBillToAttn());
            row.getCell(6).setCellValue((invoice.getShipToAttn() == null) ? "" : invoice.getShipToAttn());

            startRow += 2;
            row = invoiceSheet.getRow(startRow);
            row.getCell(4).setCellValue((invoice.getValidityTerms() == null) ? "" : invoice.getValidityTerms());
            row.getCell(6).setCellValue((invoice.getCustomerNumber() == null) ? "" : invoice.getCustomerNumber());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(4).setCellValue((invoice.getOrderDate() == null) ? "" : invoice.getOrderDate().replace("-", "/"));
            row.getCell(6).setCellValue((invoice.getDeliveryDate() == null) ? "" : invoice.getDeliveryDate().replace("-", "/"));

            row = invoiceSheet.getRow(++startRow);
            row.getCell(4).setCellValue((invoice.getOrderedBy() == null) ? "" : invoice.getOrderedBy());
            row.getCell(6).setCellValue((invoice.getDeliverySchedule() == null) ? "" : invoice.getDeliverySchedule());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(4).setCellValue((invoice.getPurchaseOrderNumber() == null) ? "" : invoice.getPurchaseOrderNumber());
            row.getCell(6).setCellValue((invoice.getAirwayBillNumber() == null) ? "" : invoice.getAirwayBillNumber());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(4).setCellValue((invoice.getSalesOrderNumber() == null) ? "" : invoice.getSalesOrderNumber());
            row.getCell(6).setCellValue((invoice.getVatRegistrationNumber() == null) ? "" : invoice.getVatRegistrationNumber());

            row = invoiceSheet.getRow(++startRow);
            row.getCell(4).setCellValue((invoice.getSalesPerson() == null) ? "" : invoice.getSalesPerson());
            row.getCell(6).setCellValue((invoice.getReference() == null) ? "" : invoice.getReference());

            startRow = 30;
            for (InvoiceEntry entry : invoiceEntries) {
                row = invoiceSheet.getRow(startRow);
                row.getCell(1).setCellValue(entry.getRef() + "");
                row.getCell(2).setCellValue(entry.getQuantity() + "");
                row.getCell(3).setCellValue(entry.getDescription());
                row.getCell(5).setCellValue(entry.getQuantity());
                row.getCell(6).setCellValue(entry.getTotalAmount());
                startRow++;
            }
            try {
                FileOutputStream fileOut = new FileOutputStream(saveFile);
                invoiceWorkBook.write(fileOut);
                fileOut.close();
                JOptionPane.showMessageDialog(null, "Invoice successfully exported!");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
