/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.models;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import longbridge.invoicer.control.InvoiceEntryJpaController;
import longbridge.invoicer.control.InvoiceJpaController;
import longbridge.invoicer.control.LongbridgeInvoicer;
import longbridge.invoicer.control.NewInvoice;

/**
 *
 * @author ACER
 */
public class DataProcesses {
    
    private static final String username = "root";
    private static final String password = "";
    public static ArrayList<Invoice> rowData;
    public static final String[] invoiceColumns = {"id", "invoiceNumber", "billTo1", "billto2", "billTo3", "billTo4", "billTo5", "billToattn",
        "shipTo1", "shipTo2", "shipTo3", "shipTo4", "shipTo5", "shipToAttn", "validityTerms", "orderDate", "orderedBy", "purchaseOrderNumber",
        "salesOrderNumber", "salesPerson", "customerNumber", "deliveryDate", "deliverySchedule", "airwayBillnumber", "vatRegistrationNumber", "reference"};
    
    static {
        //Initialize the array data
        rowData = new ArrayList();
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataProcesses.class.getName()).log(Level.SEVERE, null, ex);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Longbridge_InvoicerPU");
        EntityManager entitymanager = emf.createEntityManager();
        String queryString = null;
        queryString = "SELECT I FROM Sequence I WHERE I.SEQ_NAME='INVOICE_SEQ'";
        Query query = null;
        try{
            query = entitymanager.createQuery(queryString);
        }catch(Exception exc){
            JOptionPane.showMessageDialog(null, "Error fetching from the database. Contact support", "DB Error", JOptionPane.ERROR_MESSAGE);
        }
        LongbridgeInvoicer.currentInvoiceNumber = (Sequence) query.getResultList().get(0);
    }

    /**
     * This adds a new invoice to the database
     *
     * @param invoice The <code>Invoice</code> object to be added
     * @param entries The <code>LIst</code> object that contains all the
     * <code>InvoiceEntry</code> objects
     * @param source A reference to the NewInvoicePanel for clearing the fields
     */
    public static void addNewInvoice(Invoice invoice, List<InvoiceEntry> entries, NewInvoice source) {
        //Create controllers for the add transaction
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Longbridge_InvoicerPU");
        InvoiceJpaController invoiceController = new InvoiceJpaController(emf);
        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("Longbridge_InvoicerPU");
        InvoiceEntryJpaController invoiceEntryController = new InvoiceEntryJpaController(emf2);
        EntityManagerFactory emf3 = Persistence.createEntityManagerFactory("Longbridge_InvoicerPU");
        EntityManager sequenceController = emf3.createEntityManager();
        
        //Add the invoice to the db
        sequenceController.getTransaction().begin();
        sequenceController.persist(invoice);
        
        //Add all the invoice entries
        for (InvoiceEntry invoiceEntry : entries) {
            invoiceEntry.setInvoiceid(invoice.getId());
            sequenceController.persist(invoiceEntry);
        }
        //Update the invoice number
        LongbridgeInvoicer.currentInvoiceNumber.updateSequence();
        sequenceController.merge(LongbridgeInvoicer.currentInvoiceNumber);
        sequenceController.getTransaction().commit();
        invoice.setId(null);
        //Clear the form
        source.clearFields();
        //Show success message
        JOptionPane.showMessageDialog(source, "Invoice successfully created!", "Invoice Created", JOptionPane.INFORMATION_MESSAGE);
    }


    /**
     * This loads up the invoices from the database and stores them for
     * retrieval by the Invoice JTable.
     *
     * @param filter
     * @param filterField
     */
    public static void loadData() {
        rowData.clear();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Longbridge_InvoicerPU");
        EntityManager entitymanager = emf.createEntityManager();
        String queryString = null;
        queryString = "SELECT I FROM Invoice I";
        System.out.println(queryString);
        Query query = entitymanager.createQuery(queryString);
        rowData.addAll(query.getResultList());
    }
    
    public static List<InvoiceEntry> getInvoiceEntries(Long invoiceid){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Longbridge_InvoicerPU");
        EntityManager entitymanager = emf.createEntityManager();
       Query query = entitymanager.createQuery(String.format("Select I from InvoiceEntry I where I.invoiceid = '%s'", invoiceid));
        return query.getResultList();
    }
}