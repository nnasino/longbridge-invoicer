/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.models;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author chigozirim
 */
@Entity
@Table(name = "INVOICE")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int invoiceNumber;

    private String dateCreated;

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }
    private String billTo1;
    private String billTo2;
    private String billTo3;
    private String billTo4;
    private String billTo5;
    private String billToAttn;
    private String shipTo1;
    private String shipTo2;
    private String shipTo3;
    private String shipTo4;
    private String shipTo5;
    private String shipToAttn;
    private String validityTerms;
    private String orderDate;
    private String orderedBy;
    private String purchaseOrderNumber;
    private String salesOrderNumber;
    private String salesPerson;
    private String customerNumber;
    private String deliveryDate;
    private String deliverySchedule;
    private String airwayBillNumber;
    private String vatRegistrationNumber;
    private String reference;

    public String getColumn(int column) {
        switch (column) {
            case 0:
                return id + "";
            case 1:
                return invoiceNumber + "";
            case 2:
                return billTo1;
            case 3:
                return billTo2;
            case 4:
                return billTo3;
            case 5:
                return billTo4;
            case 6:
                return billTo5;
            case 7:
                return billToAttn;
            case 8:
                return shipTo1;
            case 9:
                return shipTo2;
            case 10:
                return shipTo3;
            case 11:
                return shipTo4;
            case 12:
                return shipTo5;
            case 13:
                return shipToAttn;
            case 14:
                return validityTerms;
            case 15:
                return orderDate;
            case 16:
                return orderedBy;
            case 17:
                return purchaseOrderNumber;
            case 18:
                return salesOrderNumber;
            case 19:
                return salesPerson;
            case 20:
                return customerNumber;
            case 21:
                return deliveryDate;
            case 22:
                return deliverySchedule;
            case 23:
                return airwayBillNumber;
            case 24:
                return vatRegistrationNumber;
            case 25:
                return reference;
            default:
                return "";
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getBillTo1() {
        return billTo1;
    }

    public void setBillTo1(String billTo1) {
        this.billTo1 = billTo1;
    }

    public String getBillTo2() {
        return billTo2;
    }

    public void setBillTo2(String billTo2) {
        this.billTo2 = billTo2;
    }

    public String getBillTo3() {
        return billTo3;
    }

    public void setBillTo3(String billTo3) {
        this.billTo3 = billTo3;
    }

    public String getBillTo4() {
        return billTo4;
    }

    public void setBillTo4(String billTo4) {
        this.billTo4 = billTo4;
    }

    public String getBillTo5() {
        return billTo5;
    }

    public void setBillTo5(String billTo5) {
        this.billTo5 = billTo5;
    }

    public String getBillToAttn() {
        return billToAttn;
    }

    public void setBillToAttn(String billToAttn) {
        this.billToAttn = billToAttn;
    }

    public String getShipTo1() {
        return shipTo1;
    }

    public void setShipTo1(String shipTo1) {
        this.shipTo1 = shipTo1;
    }

    public String getShipTo2() {
        return shipTo2;
    }

    public void setShipTo2(String shipTo2) {
        this.shipTo2 = shipTo2;
    }

    public String getShipTo3() {
        return shipTo3;
    }

    public void setShipTo3(String shipTo3) {
        this.shipTo3 = shipTo3;
    }

    public String getShipTo4() {
        return shipTo4;
    }

    public void setShipTo4(String shipTo4) {
        this.shipTo4 = shipTo4;
    }

    public String getShipTo5() {
        return shipTo5;
    }

    public void setShipTo5(String shipTo5) {
        this.shipTo5 = shipTo5;
    }

    public String getShipToAttn() {
        return shipToAttn;
    }

    public void setShipToAttn(String shipToAttn) {
        this.shipToAttn = shipToAttn;
    }

    public String getValidityTerms() {
        return validityTerms;
    }

    public void setValidityTerms(String validityTerms) {
        this.validityTerms = validityTerms;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(String orderedBy) {
        this.orderedBy = orderedBy;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getSalesOrderNumber() {
        return salesOrderNumber;
    }

    public void setSalesOrderNumber(String salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliverySchedule() {
        return deliverySchedule;
    }

    public void setDeliverySchedule(String deliverySchedule) {
        this.deliverySchedule = deliverySchedule;
    }

    public String getAirwayBillNumber() {
        return airwayBillNumber;
    }

    public void setAirwayBillNumber(String airwayBillNumber) {
        this.airwayBillNumber = airwayBillNumber;
    }

    public String getVatRegistrationNumber() {
        return vatRegistrationNumber;
    }

    public void setVatRegistrationNumber(String vatRegistrationNumber) {
        this.vatRegistrationNumber = vatRegistrationNumber;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invoice)) {
            return false;
        }
        Invoice other = (Invoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "longbridge.invoicer.models.Invoice[ id=" + id + " ]";
    }

}
