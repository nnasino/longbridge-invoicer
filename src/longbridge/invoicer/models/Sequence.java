/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.eclipse.persistence.annotations.PrimaryKey;

/**
 *
 * @author chigozirim
 */
@Entity
public class Sequence implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String SEQ_NAME;
    
    private int SEQ_COUNT;

    public String getSEQ_NAME() {
        return SEQ_NAME;
    }

    public void setSEQ_NAME(String SEQ_NAME) {
        this.SEQ_NAME = SEQ_NAME;
    }

    public int getSEQ_COUNT() {
        return SEQ_COUNT;
    }

    public void setSEQ_COUNT(int SEQ_COUNT) {
        this.SEQ_COUNT = SEQ_COUNT;
    }

  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (SEQ_NAME != null ? SEQ_NAME.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sequence)) {
            return false;
        }
        Sequence other = (Sequence) object;
        return this.SEQ_NAME.equals(other.SEQ_NAME);
       
    }

    @Override
    public String toString() {
        return "longbridge.invoicer.models.Sequence[ id=" + SEQ_NAME + " ]";
    }
    
    public void updateSequence(){
        SEQ_COUNT++;
    }
}
