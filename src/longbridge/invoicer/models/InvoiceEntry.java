/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author chigozirim
 */
@Entity
public class InvoiceEntry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int ref;
    private short quantity;
    private String description;
    private double unitPrice;
    private String priceCurrency;
    private double totalAmount;

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
    private Long invoiceid;

    public String getColumn(int column) {
        try {
            switch (column) {
                case 0:
                    return ref + "";
                case 1:
                    return quantity + "";
                case 2:
                    return description;
                case 3:
                    return priceCurrency;
                case 4:
                     return unitPrice + "";
                case 5:
                    return totalAmount + "";
                default:
                    return "";
            }
        } catch (NullPointerException exc) {
            return "";
        }
    }

    public void setColumn(int column, Object value) throws Exception {
        System.out.println(column);
        switch (column) {
            case 0:
                ref = Integer.parseInt(value.toString());
                break;
            case 1:
                quantity = Short.parseShort(value.toString());
                totalAmount = unitPrice * quantity;
                break;
            case 2:
                description = value.toString();
                break;
            case 3:
                priceCurrency = value.toString().toUpperCase();
                break;
            case 4:
                unitPrice = Double.parseDouble(value.toString());
                totalAmount = unitPrice * quantity;
                break;
        }

    }

    public long getInvoiceid() {
        return invoiceid;
    }

    public void setInvoiceid(Long invoiceid) {
        this.invoiceid = invoiceid;
    }

    public int getRef() {
        return ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    public short getQuantity() {
        return quantity;
    }

    public void setQuantity(short quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceEntry)) {
            return false;
        }
        InvoiceEntry other = (InvoiceEntry) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "longbridge.invoicer.models.InvoiceEntry[ id=" + id + " ]";
    }

}
