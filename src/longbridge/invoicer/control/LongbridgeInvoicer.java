/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.control;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import longbridge.invoicer.models.Sequence;

/**
 *
 * @author chigozirim
 */
public class LongbridgeInvoicer {
        JFrame frame;
        public static Sequence currentInvoiceNumber;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LongbridgeInvoicer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(LongbridgeInvoicer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(LongbridgeInvoicer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(LongbridgeInvoicer.class.getName()).log(Level.SEVERE, null, ex);
        }
        LongbridgeInvoicer program = new LongbridgeInvoicer();
        program.frame = new JFrame("Netbeans IDE 8.0");
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu();
        JMenu helpMenu = new JMenu();
        JMenuItem aboutFileMenu = new JMenuItem();
        SearchInvoice searchPanel = new SearchInvoice();
        NewInvoice invoicePanel = new NewInvoice();
        JTabbedPane tabs = new JTabbedPane();
        tabs.add("New Invoice", invoicePanel);
        tabs.add("Search invoice", searchPanel);
        
        program.frame.setContentPane(tabs);
        program.frame.setSize(900, 700);
        program.frame.setLocationByPlatform(true);
        program.frame.setVisible(true);
        program.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
