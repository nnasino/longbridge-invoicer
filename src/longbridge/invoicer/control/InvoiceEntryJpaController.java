/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package longbridge.invoicer.control;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import longbridge.invoicer.exceptions.NonexistentEntityException;
import longbridge.invoicer.models.InvoiceEntry;

/**
 *
 * @author chigozirim
 */
public class InvoiceEntryJpaController implements Serializable {

    public InvoiceEntryJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InvoiceEntry invoiceEntry) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(invoiceEntry);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InvoiceEntry invoiceEntry) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            invoiceEntry = em.merge(invoiceEntry);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = invoiceEntry.getId();
                if (findInvoiceEntry(id) == null) {
                    throw new NonexistentEntityException("The invoiceEntry with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InvoiceEntry invoiceEntry;
            try {
                invoiceEntry = em.getReference(InvoiceEntry.class, id);
                invoiceEntry.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The invoiceEntry with id " + id + " no longer exists.", enfe);
            }
            em.remove(invoiceEntry);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InvoiceEntry> findInvoiceEntryEntities() {
        return findInvoiceEntryEntities(true, -1, -1);
    }

    public List<InvoiceEntry> findInvoiceEntryEntities(int maxResults, int firstResult) {
        return findInvoiceEntryEntities(false, maxResults, firstResult);
    }

    private List<InvoiceEntry> findInvoiceEntryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InvoiceEntry.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public InvoiceEntry findInvoiceEntry(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InvoiceEntry.class, id);
        } finally {
            em.close();
        }
    }

    public int getInvoiceEntryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InvoiceEntry> rt = cq.from(InvoiceEntry.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
