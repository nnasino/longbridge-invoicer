------------- SQLite3 Dump File -------------

-- ------------------------------------------
-- Dump of "INVOICEENTRY"
-- ------------------------------------------

CREATE TABLE "INVOICEENTRY"(
	"ID" Numeric NOT NULL PRIMARY KEY,
	"DESCRIPTION" Text,
	"PRICECURRENCY" Text,
	"QUANTITY" Numeric,
	"REF" Numeric,
	"UNITPRICE" Numeric,
	"INVOICEID" Text,
	"TOTALAMOUNT" Numeric );


INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '302',NULL,NULL,'0','1','0','301','0' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '403','ASDFSADFSADF',NULL,'0','2','0','401','0' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '402','asdfasdf','NGN','0','1','0','401','0' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '452','asdfdsadfaf','NGN','1','1','23424','451','234124' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '453','234234aasdfasd','NGN','2','2','32000','451','32000' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '455','asdfsdafsa','USD','0','1','242','454','0' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '502','asdfsadf','NGN','23','1','2382','501','1231' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '504','asdfsad','USD','123','3','2341234','501','234132' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '503','sdlkajhsakjdf','NGN','2','2','21321','501','2341' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '506','fifa world cup germany 2092','NGN','1','1','23659','505','234252' );
INSERT INTO "INVOICEENTRY"("ID","DESCRIPTION","PRICECURRENCY","QUANTITY","REF","UNITPRICE","INVOICEID","TOTALAMOUNT") VALUES ( '552','Deloiveryasd sdaflkjhsfdfhaslkfkhdasfsd','NGN','1','1','2500','551','254234' );

-- ------------------------------------------
-- Dump of "SEQUENCE"
-- ------------------------------------------

CREATE TABLE "SEQUENCE"(
	"SEQ_NAME" Text NOT NULL PRIMARY KEY,
	"SEQ_COUNT" Numeric );


INSERT INTO "SEQUENCE"("SEQ_NAME","SEQ_COUNT") VALUES ( 'SEQ_GEN','600' );
INSERT INTO "SEQUENCE"("SEQ_NAME","SEQ_COUNT") VALUES ( 'INVOICE_SEQ','3407' );

-- ------------------------------------------
-- Dump of "invoice"
-- ------------------------------------------

CREATE TABLE "invoice"(
	"id" Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"invoicenumber" Integer NOT NULL,
	"billto1" Text,
	"billto2" Text,
	"billto3" Text,
	"billto4" Text,
	"billto5" Text,
	"billtoattn" Text,
	"shipto1" Text,
	"shipto2" Text,
	"shipto3" Text,
	"shipto4" Text,
	"shipto5" Text,
	"shiptoattn" Text,
	"validityterms" Text,
	"orderdate" Date,
	"orderedby" Text,
	"purchaseordernumber" Text,
	"salesordernumber" Text,
	"salesperson" Text,
	"customernumber" Text,
	"deliverydate" Date,
	"deliveryschedule" Text,
	"airwaybillnumber" Text,
	"vatregistrationnumber" Text,
	"reference" Text,
	"datecreated" Date,
CONSTRAINT "unique_invoice_number" UNIQUE ( "invoicenumber" ) );

CREATE INDEX "index_invoice_number" ON "invoice"( "invoicenumber" );

INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 1,121,'the','asdf','asdf','asdf','kljh','klj','hlkjh','lkjh','lkjh','lkjh','klh','lkj','hlkjh','2012-09-21','h','kj','hlkj','hlkjh','lkjh','2019-09-09','lkjh','lkjh','lkjh','lkjh','2013-02-03' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 2,234244234,'david','david',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2012-02-09','davido','david seaman','alvaro david',NULL,NULL,'2019-08-23',NULL,'Resdfaasd',NULL,NULL,'2013-09-23' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 3,83294823,'DAVID','david','david','david','david','','','','','','','','','2012-12-22','','','','','','2014-09-24','','','','','2012-08-23' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 51,0,'ljkh','kjh','kljh','kljh','kljh','kjlh','kljh','kjh','lkjh','lkjh','lkjh','lkjh','lkjh','2012-09-23','lkjh','lkjh','h','wer','kljh','2012-03-21','lkjh','lkj','sd','wew','2012-09-23' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 151,3300,'Trench','Real','Analysis','Department ','of ','ihiagw','Computer Science','School ','of sc','sciences','Futo','','Trench','1427583600000','Torti Chigozirim','92349287489','jhjdsfa','Torti Chigozirim','R239823',NULL,'two days','o23423','kljhdfashfsakdjh','23423/323423/pm/23','1449615600000' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 201,3200,'asdfsdalkhj','kjh','klj','hlkjh','lkjh','lkjh','lkjh','lkjh','lkjh','lkjh','lkjh','lkjh','lkjh','2015-12-23','dasdfkjh','lkjh','kljh','kljh','lkjh',NULL,'kljh','kljh','kljh','kjh','2015-12-23' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 301,3400,'lkjhlkj','lkjlkj','lkj','lkj','lkj','lkj','lkj','lkj','lkj','lkj','lkj','lkj','lkj','2012-02-21','asdf','kjh','kjh','kjh','lkj',NULL,'kjh','kjh','kjh','kjh','2012-02-21' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 401,3401,'lasjfsdlkj',';lkj','l;kj','lkj','l;kj','kjh','l;kj','lkj','ljkh','lkjh','kljh','lkjh','klj','2015-09-23','lk;jhjkh','kljh','lkjh','lkjh','hlkjh',NULL,'kjh','lkjh','kljh','lkjh','2015-09-23' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 451,3402,'kjlhjklh','lkjh','lkjh','kjh','kljh','','lkjh','lkj','hlk','jh','lkjh','lkjh','lkjh','2015-03-25','kljhlkjh','lkjh','lkjh','kljh','lkjh',NULL,'kljh','kljh','lkjh','lkjh','2015-03-25' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 454,3403,'asdfsdaf','asdfsadfas','asdfsadf','asdfsa','sadfa','asdfsa','asfad','asdfa','asdfasd','asdfas','asdfasd','asdsfas','adfasf','2015-03-25','kjhkjh','kljh','hgf','iouy','dfas',NULL,'kjh','hgf','oiuy','khklj','2015-03-25' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 501,3404,'jlkh','jhbj','hjkhh','jhui','huh','lkuhjkl','lku','huih','oiuh','lihjlkgf','yfg','ykutfyg','kjgbhj','2019-02-21','lkajfkljsadf','lkj','h','jkhjkh','fghj',NULL,'j','kjklh','jklh','kljh','2019-02-21' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 505,3405,'Round up the footballing','nations','for a ','tournment','','will','What ','do we ','call ','this tournament','we ','call','it ','2019-02-21','Fifa','9873897','98789787723','Torti chigozirm','the',NULL,'world cup','987y23897234','9876786','12313/231/PMG/232','2019-02-21' );
INSERT INTO "invoice"("id","invoicenumber","billto1","billto2","billto3","billto4","billto5","billtoattn","shipto1","shipto2","shipto3","shipto4","shipto5","shiptoattn","validityterms","orderdate","orderedby","purchaseordernumber","salesordernumber","salesperson","customernumber","deliverydate","deliveryschedule","airwaybillnumber","vatregistrationnumber","reference","datecreated") VALUES ( 551,3406,'dsaflkkj','l;kjj','l;jk','kjjh','lkjh','jh','uh','hkj','hh','jh','jkh','hh','hj','2014-02-12','sdf;osdjf','kljh','lkjh','lkjh','hkjlh','2014-02-12','l;kjh','lkjh','lkjh','kljh','2014-02-12' );

-- ------------------------------------------
-- Dump of "invoice_entry"
-- ------------------------------------------

CREATE TABLE "invoice_entry"(
	"description" Text NOT NULL,
	"id" Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"invoice_id" Integer NOT NULL,
	"price_currency" Text NOT NULL,
	"quantity" Integer NOT NULL,
	"ref" Integer NOT NULL,
	"total_amount" Double NOT NULL,
	"unit_price" Double NOT NULL,
	CONSTRAINT " lnk_invoice_invoice" FOREIGN KEY ( "invoice_id" ) REFERENCES "invoice"( "id" )
		ON DELETE Restrict
		ON UPDATE Cascade
,
CONSTRAINT "unique_id" UNIQUE ( "id" ) );

CREATE INDEX "index_id" ON "invoice_entry"( "id" );

